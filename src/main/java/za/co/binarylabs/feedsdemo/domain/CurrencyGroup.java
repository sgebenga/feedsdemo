package za.co.binarylabs.feedsdemo.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 3:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class CurrencyGroup {
    private String currencyGroupName;
    private String SWIFTBIC;
    private String referenceCurrency;
    private String lastUpdatedDateTime;
    List<CurrencyGroupCurrency> currencyGroupCurrencyList = new ArrayList<CurrencyGroupCurrency>();

    public String getCurrencyGroupName() {
        return currencyGroupName;
    }

    public void setCurrencyGroupName(String currencyGroupName) {
        if (this.currencyGroupName != null) {
            return;
        }
        this.currencyGroupName = currencyGroupName;
    }

    public String getSWIFTBIC() {
        return SWIFTBIC;
    }

    public void setSWIFTBIC(String SWIFTBIC) {
        if (this.SWIFTBIC != null) {
            return;
        }
        this.SWIFTBIC = SWIFTBIC;
    }

    public String getReferenceCurrency() {
        return referenceCurrency;
    }

    public void setReferenceCurrency(String referenceCurrency) {
        if (this.referenceCurrency != null) {
            return;
        }
        this.referenceCurrency = referenceCurrency;
    }

    public String getLastUpdatedDateTime() {
        return lastUpdatedDateTime;
    }

    public void setLastUpdatedDateTime(String lastUpdatedDateTime) {
        if (this.lastUpdatedDateTime != null) {
            return;
        }
        this.lastUpdatedDateTime = lastUpdatedDateTime;
    }

    public List<CurrencyGroupCurrency> getCurrencyGroupCurrencyList() {
        return currencyGroupCurrencyList;
    }

    public void setCurrencyGroupCurrencyList(List<CurrencyGroupCurrency> currencyGroupCurrencyList) {
        this.currencyGroupCurrencyList = currencyGroupCurrencyList;
    }

    @Override
    public String toString() {
        return "CurrencyGroup{" +
                "currencyGroupName='" + currencyGroupName + '\'' +
                ", SWIFTBIC='" + SWIFTBIC + '\'' +
                ", referenceCurrency='" + referenceCurrency + '\'' +
                ", lastUpdatedDateTime='" + lastUpdatedDateTime + '\'' +
                ", currencyGroupCurrencyList=" + currencyGroupCurrencyList +
                '}';
    }
}


