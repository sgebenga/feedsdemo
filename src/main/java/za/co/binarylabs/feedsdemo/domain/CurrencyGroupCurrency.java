package za.co.binarylabs.feedsdemo.domain;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 3:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class CurrencyGroupCurrency {
    private String currency;

    private String unit;
    private String buyRate;
    private String midRate;
    private String sellRate;
    private String directIndirectIndicator;
    private String lastUpdatedDateTime;

    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        if (this.currency != null) {
            return;
        }
        this.currency = currency;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        if (this.unit != null) {
            return;
        }
        this.unit = unit;
    }

    public String getBuyRate() {
        return buyRate;
    }

    public void setBuyRate(String buyRate) {
        if (this.buyRate != null) {
            return;
        }
        this.buyRate = buyRate;
    }

    public String getMidRate() {
        return midRate;
    }

    public void setMidRate(String midRate) {
        if (this.midRate != null) {
            return;
        }
        this.midRate = midRate;
    }

    public String getSellRate() {
        return sellRate;
    }

    public void setSellRate(String sellRate) {
        if (this.sellRate != null) {
            return;
        }
        this.sellRate = sellRate;
    }

    public String getDirectIndirectIndicator() {
        return directIndirectIndicator;
    }

    public void setDirectIndirectIndicator(String directIndirectIndicator) {
        if (this.directIndirectIndicator != null) {
            return;
        }
        this.directIndirectIndicator = directIndirectIndicator;
    }

    public String getLastUpdatedDateTime() {
        return lastUpdatedDateTime;
    }

    public void setLastUpdatedDateTime(String lastUpdatedDateTime) {
        if (this.lastUpdatedDateTime != null) {
            return;
        }
        this.lastUpdatedDateTime = lastUpdatedDateTime;
    }

    @Override
    public String toString() {
        return "CurrencyGroupCurrency{" +
                "currency='" + currency + '\'' +
                ", unit='" + unit + '\'' +
                ", buyRate='" + buyRate + '\'' +
                ", midRate='" + midRate + '\'' +
                ", sellRate='" + sellRate + '\'' +
                ", directIndirectIndicator='" + directIndirectIndicator + '\'' +
                ", lastUpdatedDateTime='" + lastUpdatedDateTime + '\'' +
                '}';
    }
}
