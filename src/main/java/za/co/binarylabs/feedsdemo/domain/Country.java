package za.co.binarylabs.feedsdemo.domain;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class Country {
    private String code;
    private CurrencyGroup currencyGroup;

    public Country() {

    }

    public Country(String code, CurrencyGroup currencyGroup) {
        this.code = code;
        this.currencyGroup = currencyGroup;
    }

    public CurrencyGroup getCurrencyGroup() {
        return currencyGroup;
    }

    public void setCurrencyGroup(CurrencyGroup currencyGroup) {
        this.currencyGroup = currencyGroup;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        if (this.code != null) {
            return;
        }
        this.code = code;
    }

    @Override
    public String toString() {
        return "Country{" +
                "code='" + code + '\'' +
                ", currencyGroup=" + currencyGroup +
                '}';
    }
}
