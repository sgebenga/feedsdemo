package za.co.binarylabs.feedsdemo.domain;

import za.co.binarylabs.feedsdemo.domain.CurrencyFeed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class CurrencyReport {
    private List<CurrencyFeed> currencies = new ArrayList<CurrencyFeed>();

    public CurrencyReport(List<CurrencyFeed> currencies) {
        this.currencies = currencies;
    }
}
