package za.co.binarylabs.feedsdemo.domain;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 5:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class CurrencyFeed {
    private String country;
    private String countryCurrency;
    private String usdCurrency;
    private String buyRate;
    private String sellRate;
    private String lastUpdated;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCurrency() {
        return countryCurrency;
    }

    public void setCountryCurrency(String countryCurrency) {
        this.countryCurrency = countryCurrency;
    }

    public String getUsdCurrency() {
        return usdCurrency;
    }

    public void setUsdCurrency(String usdCurrency) {
        this.usdCurrency = usdCurrency;
    }

    public String getBuyRate() {
        return buyRate;
    }

    public void setBuyRate(String buyRate) {
        this.buyRate = buyRate;
    }

    public String getSellRate() {
        return sellRate;
    }

    public void setSellRate(String sellRate) {
        this.sellRate = sellRate;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
