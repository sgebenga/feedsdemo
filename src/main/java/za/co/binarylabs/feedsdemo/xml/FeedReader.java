package za.co.binarylabs.feedsdemo.xml;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 9:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class FeedReader {
    public static final String DEFAULT_FEED_URL = "http://feeds.standardbank.com/standimg/Campaigns/Common/fxRates.xml";

    public static Document read(String link) throws Exception {
        String url = (link == null) ? DEFAULT_FEED_URL : link;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        return factory.newDocumentBuilder().parse(new URL(url).openStream());

    }
}
