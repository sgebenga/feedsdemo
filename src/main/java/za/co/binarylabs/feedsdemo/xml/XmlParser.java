package za.co.binarylabs.feedsdemo.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.TreeWalker;
import za.co.binarylabs.feedsdemo.domain.Country;
import za.co.binarylabs.feedsdemo.domain.CurrencyGroup;
import za.co.binarylabs.feedsdemo.domain.CurrencyGroupCurrency;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 9:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class XmlParser {

    private static final String RETURN = "return";
    private static final String ISO_COUNTRY_CODE = "ISOCountryCode";
    private static final String CURRENCY_GROUP_NAME = "CurrencyGroupName";
    private static final String SWIFTBIC = "SWIFTBIC";
    private static final String REFERENCE_CURRENCY = "ReferenceCurrency";
    private static final String LAST_UPDATED_TIME = "LastUpdatedDateTime";
    private static final String CURRENCY = "Currency";
    private static final String UNIT = "Unit";
    private static final String MIDRATE = "MidRate";
    private static final String SELLRATE = "SellRate";
    private static final String BUYRATE = "BuyRate";
    private static final String DIRECTINDERICTINDICATOR = "DirectIndirectIndicator";

    public static void parse(Document document, List<Country> countries) {
        Node root = document.getDocumentElement();
        int whatToShow = NodeFilter.SHOW_ALL;
        NodeFilter nodefilter = null;
        boolean expandreferences = false;

        DocumentTraversal traversal = (DocumentTraversal) document;

        TreeWalker walker = traversal.createTreeWalker(root,
                whatToShow,
                nodefilter,
                expandreferences);
        Node thisNode = null;

        thisNode = walker.nextNode();
        Country country = null;
        String nodeName = null;
        CurrencyGroupCurrency currencyGroupCurrency = null;
        while (thisNode != null) {
            if (thisNode.getNodeType() == thisNode.ELEMENT_NODE) {
                nodeName = thisNode.getNodeName();
                System.out.print(nodeName + " ");
                if (RETURN.equalsIgnoreCase(nodeName)) {
                    country = new Country();
                    country.setCurrencyGroup(new CurrencyGroup());
                    countries.add(country);

                }
                if ("CurrencyGroupCurrency".equalsIgnoreCase(nodeName)) {
                    currencyGroupCurrency = new CurrencyGroupCurrency();
                    country.getCurrencyGroup().getCurrencyGroupCurrencyList().add(currencyGroupCurrency);
                }

            } else if (thisNode.getNodeType() == thisNode.TEXT_NODE) {
                String nodeValue = thisNode.getNodeValue();
                System.out.print(nodeValue);
                if (country != null) {
                    // System.out.println("Country not null");
                    CurrencyGroup currencyGroup = country.getCurrencyGroup();
                    if (ISO_COUNTRY_CODE.equalsIgnoreCase(nodeName)) {
                        country.setCode(nodeValue);
                    }
                    if (CURRENCY_GROUP_NAME.equalsIgnoreCase(nodeName)) {
                        currencyGroup.setCurrencyGroupName(thisNode.getNodeValue());
                    }
                    if (SWIFTBIC.equalsIgnoreCase(nodeName)) {
                        currencyGroup.setSWIFTBIC(thisNode.getNodeValue());
                    }
                    if (REFERENCE_CURRENCY.equalsIgnoreCase(nodeName)) {
                        currencyGroup.setReferenceCurrency(thisNode.getNodeValue());
                    }
                    if (LAST_UPDATED_TIME.equalsIgnoreCase(nodeName)) {
                        currencyGroup.setLastUpdatedDateTime(thisNode.getNodeValue());
                    }
                    if (currencyGroupCurrency != null) {
                        if (CURRENCY.equalsIgnoreCase(nodeName)) {
                            currencyGroupCurrency.setCurrency(thisNode.getNodeValue());
                        }
                        if (UNIT.equalsIgnoreCase(nodeName)) {
                            currencyGroupCurrency.setUnit(thisNode.getNodeValue());
                        }
                        if (MIDRATE.equalsIgnoreCase(nodeName)) {
                            currencyGroupCurrency.setMidRate(nodeValue);
                        }
                        if (SELLRATE.equalsIgnoreCase(nodeName)) {
                            currencyGroupCurrency.setSellRate(nodeValue);
                        }
                        if (BUYRATE.equalsIgnoreCase(nodeName)) {
                            currencyGroupCurrency.setBuyRate(nodeValue);
                        }
                        if (DIRECTINDERICTINDICATOR.equalsIgnoreCase(nodeName)) {
                            currencyGroupCurrency.setDirectIndirectIndicator(nodeValue);
                        }
                        if (LAST_UPDATED_TIME.equalsIgnoreCase(nodeName)) {
                            currencyGroupCurrency.setLastUpdatedDateTime(nodeValue);
                        }
                    }

                }
            }
            thisNode = walker.nextNode();
        }


    }
}
