package za.co.binarylabs.feedsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.w3c.dom.*;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.TreeWalker;
import za.co.binarylabs.feedsdemo.domain.Country;
import za.co.binarylabs.feedsdemo.domain.CurrencyReport;
import za.co.binarylabs.feedsdemo.json.JsonGenerator;
import za.co.binarylabs.feedsdemo.xml.FeedReader;
import za.co.binarylabs.feedsdemo.xml.XmlParser;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@SpringBootApplication
public class FeedsDemoApplication {
    private static final Logger LOGGER = Logger.getLogger("FeedsDemo");


    private static final String CURRENCY_GROUP_CURRENCY = "CurrencyGroupCurrency";


    public static void main(String[] args) {
        SpringApplication.run(FeedsDemoApplication.class, args);
        try {
            Document document = FeedReader.read(FeedReader.DEFAULT_FEED_URL);

            DocumentTraversal traversal = (DocumentTraversal) document;

            TreeWalker walker = traversal.createTreeWalker(document.getDocumentElement(),
                    NodeFilter.SHOW_ELEMENT, null, false);
            List<Country> countries = new ArrayList<Country>();

            XmlParser.parse(document, countries);
            CurrencyReport report = CurrencyFeedFilter.filterByCountryCode(countries, "USD");
            JsonGenerator.toJson(report);
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }


    private static void print(List<Country> countries) {
        for (Country c : countries) {
            System.out.println(c);
        }
    }


}

