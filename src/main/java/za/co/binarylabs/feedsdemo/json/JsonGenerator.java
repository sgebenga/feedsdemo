package za.co.binarylabs.feedsdemo.json;

import com.google.gson.Gson;
import za.co.binarylabs.feedsdemo.domain.CurrencyReport;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 9:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class JsonGenerator {

    private static final Logger LOGGER = Logger.getLogger("JsonGenerator");

    /* generates json from currency report */
    public static void toJson(CurrencyReport report) {
        Gson gson = new Gson();
        String json = gson.toJson(report);

        try {
            FileWriter writer = new FileWriter("feed.json");
            writer.write(json);
            writer.close();

        } catch (IOException e) {
            LOGGER.warning(e.toString());
        }
    }
}
