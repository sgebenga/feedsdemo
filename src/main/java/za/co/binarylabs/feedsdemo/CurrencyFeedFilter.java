package za.co.binarylabs.feedsdemo;

import za.co.binarylabs.feedsdemo.domain.Country;
import za.co.binarylabs.feedsdemo.domain.CurrencyFeed;
import za.co.binarylabs.feedsdemo.domain.CurrencyGroupCurrency;
import za.co.binarylabs.feedsdemo.domain.CurrencyReport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 9:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class CurrencyFeedFilter {

    /* selects currency group for a particular code */
    public static CurrencyReport filterByCountryCode(List<Country> countries, String code) {
        List<CurrencyFeed> currencyFeeds = new ArrayList<CurrencyFeed>();
        for (Country country : countries) {
            String countryCode = country.getCode();
            CurrencyFeed currencyFeed = new CurrencyFeed();
            currencyFeed.setCountry(country.getCurrencyGroup().getCurrencyGroupName());
            currencyFeed.setCountryCurrency(countryCode);

            for (CurrencyGroupCurrency cgc : country.getCurrencyGroup().getCurrencyGroupCurrencyList()) {
                if (code.equals(cgc.getCurrency())) {
                    currencyFeed.setBuyRate(cgc.getBuyRate());
                    currencyFeed.setLastUpdated(cgc.getLastUpdatedDateTime());
                    currencyFeed.setSellRate(cgc.getSellRate());
                    currencyFeed.setUsdCurrency(code);
                    currencyFeeds.add(currencyFeed);
                }
            }
        }
        return new CurrencyReport(currencyFeeds);

    }
}
