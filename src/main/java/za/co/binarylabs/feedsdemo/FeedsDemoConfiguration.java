package za.co.binarylabs.feedsdemo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2016/01/14
 * Time: 12:57 PM
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class FeedsDemoConfiguration {

    @Bean
    CommandLineRunner init(){
        return new CommandLineRunner() {
            @Override
            public void run(String... strings) throws Exception {
              System.out.println("running");
            }
        }  ;
    }
}
